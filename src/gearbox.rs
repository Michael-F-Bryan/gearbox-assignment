
#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub struct Gearbox {
    pub n1: u32,
    pub n2: u32,
    pub n3: u32,
    pub n4: u32,
}

impl Gearbox {
    pub fn ratio(&self) -> f32 {
        let top = self.n1 * self.n3;
        let bottom = self.n2 * self.n4;
        (top as f32) / (bottom as f32)
    }
}

impl From<(u32, u32, u32, u32)> for Gearbox {
    fn from(other: (u32, u32, u32, u32)) -> Gearbox {
        let (n1, n2, n3, n4) = other;
        Gearbox { n1, n2, n3, n4 }
    }
}
