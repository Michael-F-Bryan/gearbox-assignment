use std::time::{Duration, Instant};


pub struct Timer {
    start: Instant,
    checkpoints: Vec<(&'static str, Duration)>,
}

impl Timer {
    pub fn new() -> Timer {
        Timer {
            start: Instant::now(),
            checkpoints: Vec::new(),
        }
    }

    pub fn checkpoint(&mut self, name: &'static str) {
        let duration = self.start.elapsed();
        self.checkpoints.push((name, duration));
    }

    pub fn checkpoints(&self) -> &[(&'static str, Duration)] {
        &self.checkpoints
    }
}
