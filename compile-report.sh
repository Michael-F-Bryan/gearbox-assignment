#!/bin/bash

set -e

TAG="registry.gitlab.com/michael-f-bryan/gearbox-assignment"
CMD="latexmk -pdf --output-directory=build gearbox-assignment.tex"

$CMD "$@"