# Gearbox Assignment

The analysis and design of a gearbox.

[**Rendered**](https://Michael-F-Bryan.gitlab.io/gearbox-assignment)


## Getting Started

Check out the repository:

```
$ git clone git@gitlab.com:Michael-F-Bryan/gearbox-assignment.git
```

You'll need to have LaTeX and the Rust compiler installed, the easiest way to
install Rust is via [rustup]. I'm also using a Docker image so we get 
reproducible builds, if you have docker installed you just need to run the
build script.

```
$ ./compile-report.sh
```

Otherwise you'll need to invoke `latexmk` directly.

```
$ latexmk -pdf --output-directory=build report.tex
```

The compiled document will now be at `./build/report.pdf`.

All arguments to the build script get forwarded to `latemxk`, meaning it's 
really easy to have it open up the document and then recompile and update the
viewer every time you hit save.

```
$ ./compile-report.sh -pvc
```


[rustup]: https://rustup.rs/
