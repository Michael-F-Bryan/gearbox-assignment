use std::ops::Div;
use State;
use gearbox::Gearbox;


#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct Cost {
    system_size: f32,
    distance_from_desired_ratio: f32,
}


impl Cost {
    pub fn evaluate(state: &State, gb: &Gearbox) -> Cost {
        let system_size = gb.n1 + gb.n2;
        let system_size = system_size as f32;

        let distance_from_desired_ratio =
            (state.desired_output / state.input_speed - gb.ratio()).abs();

        Cost {
            system_size,
            distance_from_desired_ratio,
        }
    }

    pub fn min_of(a: Cost, b: Cost) -> Cost {
        let cmp = |a: f32, b: f32| if a == 0.0 {
            b
        } else if b == 0.0 {
            a
        } else {
            a.min(b)
        };

        Cost {
            system_size: cmp(a.system_size, b.system_size),
            distance_from_desired_ratio: cmp(
                a.distance_from_desired_ratio,
                b.distance_from_desired_ratio,
            ),
        }
    }

    pub fn total(&self) -> f32 {
        self.system_size + self.distance_from_desired_ratio
    }
}

impl Div for Cost {
    type Output = Self;

    fn div(self, other: Cost) -> Cost {
        Cost {
            system_size: self.system_size / other.system_size,
            distance_from_desired_ratio: self.distance_from_desired_ratio
                / other.distance_from_desired_ratio,
        }
    }
}

pub fn normalize_costs(costs: &mut [(Gearbox, Cost)]) {
    let mut min = costs[0].1;

    // Find the smallest member of each cost component
    for &(_, cost) in costs.iter() {
        min = Cost::min_of(min, cost);
    }

    // then divide each item by that
    for &mut (_, ref mut cost) in costs.iter_mut() {
        *cost = *cost / min;
    }
}


pub fn sort_costs(costs: &mut [(Gearbox, Cost)]) {
    costs.sort_by(|l, r| l.1.total().partial_cmp(&r.1.total()).unwrap());
}

pub fn apply_cost_function(state: &State, candidates: &[Gearbox]) -> Vec<(Gearbox, Cost)> {
    candidates
        .into_iter()
        .cloned()
        .map(|gb| (gb, state.apply_cost_function(&gb)))
        .collect()
}
