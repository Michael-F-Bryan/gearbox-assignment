FROM ubuntu:artful

RUN apt-get update \
    && apt-get install -y --no-install-recommends ca-certificates openssl gcc make git bash curl texlive-base texlive-bibtex-extra texlive-extra-utils texlive-generic-recommended texlive-fonts-recommended texlive-font-utils texlive-latex-base texlive-latex-recommended texlive-latex-extra texlive-pictures texlive-pstricks texlive-science  perl-tk purifyeps chktex latexmk xindy fragmaster lacheck latexdiff libfile-which-perl dot2tex tipa prosper texlive-xetex \
    && apt-get install -y build-essential python3 python3-pip pandoc \
    && pip3 install numpy matplotlib jupyter \ 
    && apt-get -y autoremove \
    && apt-get -y autoclean

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH

RUN curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain 1.20.0
