#![feature(conservative_impl_trait)]

#[macro_use]
extern crate itertools;
extern crate rayon;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod constraints;
mod gearbox;
mod cost;
mod timing;

use std::fmt::{self, Display, Formatter};
use std::fs::File;
use std::path::Path;

use constraints::{is_speed_reducer, shafts_are_parallel, within_practical_geartrain_limits, Ratios};
use cost::{apply_cost_function, normalize_costs, sort_costs, Cost};
use gearbox::Gearbox;
use timing::Timer;


fn main() {
    let state = State {
        input_speed: 1890.0,
        desired_output: 90.0,
        output_margin: 3.0,
        min_teeth: 18,
        max_teeth: 300,
    };
    let ratio_limits = state.ratio_limits();

    println!("Problem space: {}", state.total_number_of_combinations());
    let mut timer = Timer::new();

    // get a lazy iterator over all the possible combinations, pass them
    // through a bunch of filters (our constraints), then collect them into
    // an array of Gearboxes (Vec<Gearbox>).
    let candidates: Vec<Gearbox> = state
        .gearbox_combinations()
        .filter(|gb| shafts_are_parallel(gb))
        .filter(|gb| is_speed_reducer(gb))
        .filter(|gb| ratio_limits.gearbox_within_bounds(gb))
        .filter(|gb| within_practical_geartrain_limits(gb))
        .collect();

    timer.checkpoint("Candidates calculated");
    println!("# candidates: {}", candidates.len());

    // Apply the cost function to our candidates and collect the results
    // into an array of (Gearbox, Cost) pairs.
    let mut costed = apply_cost_function(&state, &candidates);
    timer.checkpoint("Applied cost functions");

    // Normalize the costs by dividing each individual component of the
    // cost function by the smallest value for that component.
    normalize_costs(&mut costed);
    sort_costs(&mut costed);
    timer.checkpoint("Found the optimum combination");

    let n = 20;
    println!("Top {} candidates:", n);
    for &(gb, _) in &costed[..n] {
        println!("{:?}", gb);
    }

    println!("Timing information:");

    for &(name, duration) in timer.checkpoints() {
        println!(
            "{} ({}.{:03}s)",
            name,
            duration.as_secs(),
            duration.subsec_nanos() / 1_000_000,
        );
    }

    save(&costed, &state, "combinations.json");
}

fn save<P: AsRef<Path>>(costs: &[(Gearbox, Cost)], state: &State, dest: P) {
    #[derive(Serialize)]
    struct Temp {
        state: State,
        combinations: Vec<(Gearbox, Cost)>,
    }

    let t = Temp {
        state: state.clone(),
        combinations: costs.to_vec(),
    };

    let mut f = File::create(dest).unwrap();
    serde_json::to_writer_pretty(&mut f, &t).unwrap();
}


#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct State {
    input_speed: f32,
    desired_output: f32,
    output_margin: f32,
    min_teeth: u32,
    max_teeth: u32,
}

impl State {
    pub fn total_number_of_combinations(&self) -> PrettyNumber {
        let n = (self.min_teeth as u64 * self.max_teeth as u64).pow(4);
        PrettyNumber(n)
    }

    /// Get a lazily evaluated iterator of all the possible gearbox combinations.
    pub fn gearbox_combinations(&self) -> impl Iterator<Item = Gearbox> {
        let tooth_range = self.min_teeth..self.max_teeth + 1;
        iproduct!(
            tooth_range.clone(),
            tooth_range.clone(),
            tooth_range.clone(),
            tooth_range
        ).map(|combo| Gearbox::from(combo))
    }

    pub fn ratio_limits(&self) -> Ratios {
        let upper_ratio_limit = (self.desired_output + self.output_margin) / self.input_speed;
        let lower_ratio_limit = (self.desired_output - self.output_margin) / self.input_speed;
        Ratios::new(lower_ratio_limit, upper_ratio_limit)
    }

    fn apply_cost_function(&self, gb: &Gearbox) -> Cost {
        Cost::evaluate(self, gb)
    }
}

/// A pretty formatter which will print a number with comma separators.
#[derive(Debug, Copy, Clone)]
pub struct PrettyNumber(u64);

impl Display for PrettyNumber {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        let mut chunks = Vec::new();
        let mut n = self.0;
        while n > 0 {
            let remainder = n % 1000;
            chunks.push(remainder);
            n /= 1000;
        }

        let mut segments: Vec<String> = chunks.iter().rev().map(|s| format!("{:03}", s)).collect();
        segments[0] = segments[0].trim_left_matches('0').to_string();
        write!(f, "{}", segments.join(","))
    }
}
