\documentclass[a4paper,12pt]{article}
\usepackage[marginparwidth=25mm]{geometry}  % Give the margin more room for todonotes
\usepackage{graphicx}
\usepackage{amsmath}                  % for align
\usepackage{amssymb}                  % for \therefore
\usepackage{amsfonts}
\usepackage{xfrac}                    % for slanted fractions like 1/2
\usepackage{pdfpages}                 % insert pdf documents
\usepackage{float}                    % better control over floats and figures
\usepackage[section]{placeins}        % Stop floats from crossing sections
\usepackage{setspace}                 % custom spacing
\usepackage{booktabs}                 % pretty tables
\numberwithin{equation}{section}      % Make sure equations are numbered by section
\usepackage{hyperref}                 % Add hyperlinks to the pdf
\usepackage{cleveref}                 % add the \nameref{} command to insert the
                                      %     reference's name instead of number
\usepackage{grffile}                  % Allow you to use dotted names for graphics
\usepackage{rotating}                 % Horizontal pages
\usepackage[nottoc]{tocbibind}        % add the bibliography to the ToC
\usepackage[obeyFinal]{todonotes}
\usepackage{lscape}                   % For landscape layout


% Remove the indent at the start of a paragraph and space paragraphs out a bit
\parindent=0pt
\parskip=10pt
\renewcommand{\arraystretch}{1.75}

% Set the bibliography style  (Entwistle, 2016)
% \usepackage[round, authoryear]{natbib}
\usepackage{apacite}
\bibliographystyle{apacite}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% Add the cover page
\include{cover_page}


\section*{Executive Summary}

As part of an engineering company specializing in the manufacture of
gearboxes for industrical applications, the objective of this report is to
come up with a practical solution to satisfy customer requirements, as
detemined by the company's marketing department. The design aims to be a 
practical proposal which minimises the overall package size and effort 
required to manufacture and maintain the system.

The marketing department has determined that there is a need for a gearbox which 
can step fown from 1890 to 90 rpm, transferring 2.5kW of power with minimal 
losses.

This design incorproates an iterative approach, where intermediate solutions 
were generated, getting closer to the optimal solution at each step. It 
incorporates a two stage geartrain where the gear ratios have been determined 
by computationally checking the entire practical solution space, filtering out 
all gearbox combinations which don't satisfy the provided constraints, then 
applying a set of cost functions in order to determine the most desireable
combination of gears.

This resulted in a fairly compact gearbox (as depicted in Figure 
\ref{fig:sample}).

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{./model/Capture.JPG}
    \caption{Proposed Gearbox} \label{fig:sample}
\end{figure}

\clearpage

\tableofcontents
\clearpage

\listoffigures
\listoftables
\clearpage


\section{Introduction}

A company specialising in the manufacture of gearboxes for industrial 
applications has been approached by a company and asked to design a gearbox for 
a custom application. 

This report proposes a two-stage step down transmission capable of delivering 
the stated $2.5~kW$ while maintaining a gear ratio of 21:1 over the design 
lifetime of approximately 6 years.


\subsection{Objectives and Constraints}

The final gearbox should satisfy the following constraints: 

\begin{itemize}
    \item Have an input speed of $1890~rpm$ and output speed of $90 \pm3~rpm$
    \item Be reverted (i.e. input and output shafts are in line)
    \item Be capable of transferring $2.5~kW$
    \item Have an expected lifetime of 12480 hours (running continuously for 6 
        years at 8 hrs/day and 5 days/week) with a maximum failure rate of $2\%$
    \item Use 4 bolts on the base for mounting
    \item Minimise the size of the overall gearbox 
    \item Be designed in such a way as to facilitate assembly and disassembly 
        for maintenance
\end{itemize}


\section{Literature Review}
% A thorough review of the literature on the design,
% analysis, types and applications of gearboxes. It shall reflect the latest
% developments in the areas, such as FEA of gearing systems and
% maintenance techniques applicable to gearboxes. This section must
% demonstrate a meaningful review of a wide range of refereed journal
% papers and books in this area. References to the sources of information
% must be cited in the text. Generic comments and information taken from lecture
% notes or commercial websites will attract no marks. [Limit: Three A4 pages].

One of the most common techniques for transmitting power from one rotating shaft
to another, usually with a step up or step down in the rotatipon speed, is 
through the use of gear chains. This allows power to be transmitted with high 
efficiencies and at a large range of speeds. Due to large demand for high 
quality geartrains, there has been a lot of research into gear lifecycles and 
detecting when a gearbox is needing maintenance.


\subsection{Gearbox Analysis Techniques}

Vibration analysis of gearboxes has the ability to detect mechanical issues like
misalignment, bearing wear, broken or cracked teeth, and eccentrict shafts 
\cite{GearboxVibrationAnalysis}. Most modern techniques measure vibration of 
the gearbox casing, attempting to detect faults early on in their development 
and monitor their evolution. typically using a laser or accelerometer. 

The data is then passed through a Fourier transform to break the signal down
into its constituent frequencies and amplitudes, with more peaks potentially 
indicating a fault.

Figure \ref{fig:cracked-tooth} shows the frequency analysis of a gearbox 
containing a cracked tooth. As can be seen in the green graph, there is a 
periodic increase in vibration amplitude as the cracked tooth approaches the 
point where the two gears mesh.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{cracked-tooth.png}
    \caption[Cracked Tooth Frequency Analysis]{Cracked Tooth Frequency Analysis \cite{CrackedTooth}} 
    \label{fig:cracked-tooth}
\end{figure}

A second, completely orthogonal analysis technique in use is \textit{Finite 
Element Analysis}. In 1997, Howard et al. studied the behaviour of gears under 
mesh, constraining one and measuring the system's behaviour when a torque is 
applied to the other gear \cite{Howard}. In the study, a finite element model
of spur gears is developed based on empirical investigation, then a customised
AutoCAD program was used to transfer the model to ANSYS for simulation. 

In a different analysis Li et al. used \textit{Finite Element Analysis} to 
generate an optimum design for their gear system \cite{FeaIntegration}. This is
fairly typical practice in modern times due to high quality simulation software 
and powerful computers being relatively inexpensive.


\subsection{Bearings}

Rolling contact bearings, either containing ball bearings or rolling elements, 
are ubiquitous in modern rotating machines. A rolling contact bearing consists
of a number of balls or rolling elements within an inner and outer ring, 
sometimes contained by a \textit{cage} (a chain-like component where each "link"
goes around a single ball bearing to keep them all rolling in sync).

When faults occur in such bearings it is typically due to wear or pitting on the 
inside of one of the bearing surfaces. Standard dynamic analysis can be used to
determine the approximate frequency of impact between the balls and these 
defects, given the speed of rotation and a detailed understanding of the bearing 
surfaces \cite{BearingVibrationAnalysis}. 

The lifetime of rolling contact bearings can be greatly extended by applying a 
lubricant \cite{EssentialConcepts}. This is because the thin lubricant layer 
prevents metal-on-metal contact, preserving the inner bearing surfaces. It also
greatly increases efficiency because the only friction comes from the hydraulic 
shearing of the lubricant itself. 


\section{Gearbox Parameters}
% Clear, concise calculations of all gearbox parameters, including speed
% ratios, gear sizes, torques, forces and shaft diameters.
%
% The relevant section shall also include a review of the literature on shaft
% materials. The information in this section shall come from books,
% standards and journal articles. In-text referencing is required. [Limit: One
% A4 page].
% 
% Complete procedure for the analysis and selection of gearbox
% components, such as bearings and keys. 

The easiest way to determine the precise gearbox parameters (gear sizes,
speed ratios, shaft diameters, etc) is to formulate a set of hard
\textit{constraints} to reduce the total number of possible solutions, and a
set of desired attributes of the system which can be combined into a single
cost function. From there (with the help of computers) it is almost trivial
to check every single combination of system parameters in the problem space,
using the constraints to quickly filter out infeasible combinations and the
cost function to determine whether one combination is better than another.
This only takes 5-10 seconds on a modern laptop.

The governing parameters for this system are the number of teeth in the four
gears, $N_1$, $N_2$, $N_3$, and $N_4$. Given a particular set of gears it is 
possible to derive almost every other parameter of the system.


\subsection{Gear Ratio Calculations}

From the design brief, the gearbox's output speed must be $90 \pm 3 ~ rpm$. 
With an input speed of $1890 ~ rpm$ this first system constraint can be represented
as shown in Equation \ref{eq:desired-speed-constraint}.

\begin{align} 
    \frac{87}{1890} \le \frac{N_1}{N_2} \times \frac{N_3}{N_4} \le \frac{93}{1890} 
        \label{eq:desired-speed-constraint} 
\end{align}

Additionally, from their very definition the number of gear teeth must be a 
positive integer. 

\begin{align}
    N_1, N_2, N_3, N_4 \in \mathbb{Z}^+
\end{align}

Due to practical reasons like manufacturability and packaging, a rule of thumb 
is that no individual stage in a geartrain can have a ratio greater than 10 
(or below $\frac{1}{10}$, depending on the point of view). This provides another
constraint for the system. 

\begin{align}
    \frac{1}{10} \le \frac{N_1}{N_2} \le 10 \\
    \frac{1}{10} \le \frac{N_3}{N_4} \le 10 
\end{align}

For a particular pressure angle, $\phi$, the minimum number of teeth required 
for gears to mesh correctly can be derived using Equation \ref{eq:min-teeth}.

\begin{align}
    N_{min} = \frac{2k}{\sin^2 \phi} \label{eq:min-teeth}
\end{align}

Where $k$ is equal to $1$ for full depth teeth on a spur gear.

The most commonly used pressure angle is 20\textdegree, substituting into
Equation \ref{eq:min-teeth} shows that the minimum number of teeth for any
gear in the system is 18.

\begin{align}
    N_{min} &= \frac{2k}{\sin^2 \phi} \\
    &= \frac{2}{\sin^2 20} \\
    &= \frac{2}{0.116978} \\
    &= 17.09 \\
    N_{min} & \approx 18~teeth
\end{align}

To prevent thrust forces due to meshing gears being at an angle to each other, 
both stages of the geartrain must have an identical centre-centre distance.
This constraint ensures that the complexity and construction difficulty of the
gearbox is kept at a minimum.

From the definition of \textit{module},

\begin{align}
    m &= \frac{D}{N} \label{eq:centre-centre-distance-start} \\
    m &= \frac{2r}{N} \\
    r &= \frac{m}{2} \times N \\
    \intertext{For $N_1$ and $N_2$,}
    D_{c-c} &= r_1 + r_2 \\
    &= \frac{m}{2} \big( N_1 + N_2 \big) \\
    \therefore \frac{m}{2} \big( N_1 + N_2 \big) &= \frac{m}{2} \big( N_3 + N_4 \big) \\
    \intertext{i.e. the constraint is:}
    N_1 + N_2 &= N_3 + N_4 \label{eq:centre-centre-distance}
\end{align}

As shown in Equation \ref{eq:centre-centre-distance}, the total number of 
teeth in each stage must be constant to ensure the input/output shaft and 
countershaft are kept parallel.

Falling out from the desired output (Equation 
\ref{eq:desired-speed-constraint}) and practical geartrain ratio constraints,
another constraint is that the number of teeth in a stage must be 
\textit{increasing}. This will ensure that the speed is reduced from input
to ouput.

i.e.

\begin{align}
    N_1 < N_2, N_3 < N_4 \label{eq:reduction-gearbox}
\end{align}

While it isn't strictly necessary to be included (being a more general form of
existing constraints), by virtue of being very general and cheap to compute, 
Equation \ref{eq:reduction-gearbox} serves as a very course filter and will
help to quickly decrease the number of candidate systems. Decreasing the 
amount of time required overall to find the \textit{optimum} system.

For convenience, the system constraints are summarized in Table \ref{tab:constraints}.

\begin{table}[!htbp]
    \centering
    \caption{Overall System Constraints}
    \label{tab:constraints}
    \begin{tabular}{l l r}
        \toprule
        \# & Name & Equation \\
        \midrule
        1. & Desired Output Speed & $\frac{87}{1890} \le \frac{N_1 N_3}{N_2 N_4} \le \frac{93}{1890}$ \\
        2. & Number of Teeth is an Integer & $N_1, N_2, N_3, N_4 \in \mathbb{Z}^+$ \\
        3. & Practical Geartrain Limits & $\frac{1}{10} \le \frac{N_1}{N_2} \le 10$ \\
        4. & Minimum Number of Teeth & $N_{min} \approx 18$ \\
        5. & Parallel Shafts & $N_1 + N_2 = N_3 + N_4$ \\
        6. & Speed Reduction Gearbox & $N_1 < N_2, N_3 < N_4$ \\
        \bottomrule
    \end{tabular}
\end{table}

To help choose which gearbox combination is the most desireable, a cost 
function is created by combining weighted sums of the various attributes we 
wish to optimise for.

The largest contributor towards system size is the size of the gears being used.
The working done from Equations \ref{eq:centre-centre-distance-start} to 
\ref{eq:centre-centre-distance} show that the centre-centre distance is 
proportional to the sum of the tooth numbers for one of the stages (it doesn't 
matter \textit{which} stage because stage 1 and 2 have the same number). 
Therefore the first cost function can be represented as shown in Equation 
\ref{eq:cost-1}.

\begin{align}
    \text{Cost 1:}\quad N_1 + N_2 \label{eq:cost-1}
\end{align}

The desired gear ratio is $\frac{90}{1890}$ and although the output speed can 
be anywhere within $3~rpm$ of the desired speed, it is implied that (all things
being equal) a gearbox who's ratio is closer to the desired ratio is preferred
over one who's ratio isn't as close. Therefore the second cost function can be 
expressed as Equation \ref{eq:cost-2}.

\begin{align}
    \text{Cost 2:}\quad \Big| \frac{90}{1890} - \frac{N_1 N_3}{N_2 N_4} \Big| \label{eq:cost-2}
\end{align}

The two cost functions are summarized in Table \ref{tab:desireable-factors}.

\begin{table}[htbp]
    \centering
    \caption{Desireable Factors}
    \label{tab:desireable-factors} 
    \begin{tabular}{l l r}
        \toprule
        \# & Name & Expression to Minimise \\
        \midrule
        1. & System Size & $N_1 + N_2$ \\
        2. & Desired Ratio & $\lvert \frac{90}{1890} - \frac{N_1 N_3}{N_2 N_4} \rvert$ \\
        \bottomrule
    \end{tabular}
\end{table}

After applying the constraints from Table \ref{tab:constraints} to the
solution space (all gearboxes with tooth numbers in the range $[18, 300]$),
approximately 34236 candidate gearbox combinations are found. The cost
functions from Table \ref{tab:desireable-factors} are then combined and
applied to the candidate gearboxes to find the \textit{best} combination. 

\begin{table}[htbp]
    \centering
    \caption{Top 10 Candidates}
    \label{tab:candidates}
    \input{candidates.tex}
\end{table}

From Table \ref{tab:candidates}, the best gearbox combination appears
to have gear sizes of 21, 99, 22, and 98 to give a total number of teeth per 
stage of 120.

Gears with 98 or 99 teeth aren't standard, and as such don't exist in most
gear catalogues, but luckily the T.E.A website state that it is
possible to create custom gears in small quantities.

For a module of 4 and pressure angle of 20\textdegree{} the gear dimensions can
be found in the T.E.A catalogue \cite{TeaCatalogue}. The dimensions for tooth
numbers of 21 and 22 are already stated directly, however for the other two
gears the hub and central hole sizes can be inferred from the 100 tooth gear.

\begin{table}[htbp]
    \centering
    \caption[Gear Dimensions]{Gear Dimensions (all dimensions in mm)}
    \label{tab:gear-dimensions}
    \input{gear_dimensions.tex}
\end{table}


\subsection{Bearing Loads}

A high level schematic of the gearbox is shown in Figure \ref{fig:schematic}. 
This is used extensively in order to calculate the loads experienced by the 
bearings, and therefore shaft diameters and the bearings to be used.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{schematic.png}
    \caption[Schematic Diagram]{Schematic Diagram (not to scale)} \label{fig:schematic}
\end{figure}

The load transferred between gears and to the shaft can be determined by
knowing the power input and using the rotational definition for power, $P = T
\omega$.

\begin{align}
    P &= T \omega \\
    T &= \frac{P}{\omega} \\
    &= F r \\
    F &= \frac{T}{r} \\
    \therefore F &= \frac{P}{\omega r} \label{eq:gear-force}
\end{align}

The equivalent forces on each gear are summarized in Table \ref{tab:eq-loads}.

\begin{table}[htbp]
    \centering
    \caption{Gear Forces}
    \label{tab:eq-loads}
    \input{gear_forces.tex}
\end{table}

To determine the bearing loads, a free body diagram can be constructed using 
Figure \ref{fig:schematic} as a reference. The tangential and radial forces from
Table \ref{tab:eq-loads} translate directly to the vertical and horizontal 
forces transferred to the countershaft.

\begin{align}
    \intertext{For both horizontal and vertical components:}
    \Sigma F &= 0 \\
    F_2 + F_3 &= A + B \\
    \Sigma M_A &= 0 \\
    b \times F_2 + (a-c) \times F_3 &= a \times B \\
    B &= \frac{b \times F_2 + (a-c) \times F_3}{a} \\
    \Sigma M_B &= 0 \\
    c \times F_3 + (a-b) \times F_2 &= a \times A \\
    A &= \frac{c \times F_3 + (a-b) \times F_2}{a}
\end{align}

Solving this using $a = 140$, $b = 28$, and $c = 28$ results in the forces shown
in Table \ref{tab:countershaft-loads}. 

\begin{table}[htbp]
    \centering
    \caption{Countershaft Bearing Loads}
    \label{tab:countershaft-loads}
    \input{countershaft_loads.tex}
\end{table}

To aid in identifying which areas of the countershaft are under the greatest 
loads and bending moments, shear force and bending moment diagrams were 
constructed. This is done by calculating the total force and bending moment to 
the left of some arbitrary point, $x$, along the countershaft's axis for every 
point between 0 and its total length. Figure \ref{fig:bending-moments} graphs
the bending moment and shear forces as a function of $x$.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=0.6\textwidth]{bending-moment-diagram.png}
    \caption{Shear Force and Bending Moment Diagram} \label{fig:bending-moments}
\end{figure}


\subsection{Bearing Requirements Analysis}

The bearing to be used was determined from by referring to a SKF bearing 
catalogue, looking for the smallest (to reduce overall size) bearing which 
satisfies the desired bore size and rated load \cite{SkfCatalogue}.

The rated load, $C$, is a function of the bearing load and its effective
lifetime as shown in Equation \ref{eq:rated-load}.

\begin{align}
    C &= F_e K_a \big( \frac{L}{K_r L_R} \big)^{0.3} \label{eq:rated-load}
\end{align}

The application factor, $K_a$, for a ball bearing used for gearing is between 
$1.0$ and $1.3$ and the life adjustment factor, $K_r$, required to achieve a 
reliability of 98\% is $0.35$. The expected lifetime is the number of seconds 
in 6 years, when the gearbox is run 8 hours per day, 5 days a week. This 
evaluates to $44,928,000$ seconds.

\begin{align}
    \text{Expected Lifetime} &= \frac{secs}{hour} 
        \times \frac{hours}{day} 
        \times \frac{days}{week} 
        \times \frac{weeks}{year}
        \times years \\
    &= 3600 \times 8 \times 5 \times 52 \times 6 \\
    &= 44,928,000~seconds \\
    L &= \frac{21}{99} \times 1890 \times 44928000 \div 60 \\
    &= 300200727.273~revolutions
\end{align}

Substituting this into Equation \ref{eq:rated-load} gives:

\begin{align}
    C &= 484.8 \times 1.3 \times \big( \frac{300200727.273}{0.35 \times 90 \times 10^6} \big)^{0.3} \\
      &= 1239.6 ~ N
\end{align}

For ease of manufacturing, all four support roller bearings will be
identical. After consulting the SKF roller bearing guide, a deep groove ball
bearing with diameter $12 mm$ was chosen. This has a $C$ of $2.91~kN$,
providing a factor of safety of $2.3$. Analysing the bearing forces on the
input and output shafts is outside of the scope of this design proposal, thus
it is assumed that the plain bearings outboard of the input and output gears
($N1$ and $N4$) will have the same bore size as the gears, $20$ and $25~mm$
respectively.

\begin{table}[htb]
    \caption{Bearing Dimensions} \label{tab:bearing-dimensions}
    \begin{tabular}{lrrrrrr}
        \toprule
        & C & FOS & Model & Bore & OD & Face Width \\
        \midrule
        Support Roller Bearing & 2910 & 2.3 & W61901 & 12 & 24 & 6 \\
        Input Plain Bearing & 51000 & & GEH 20 C & 20 & 42 & 25 \\
        Output Plain Bearing & 48000 & & GEG 25 ES & 25 & 42 & 25 \\
        \bottomrule
    \end{tabular}
\end{table}


\subsection{Component Mounting}

Some of the more popular methods for mounting components to shafts involve 
retaining rings, splines, pins, set screws, and interference fits. This design 
uses profiled keys and keyways to transfer energy from the gears to their 
shafts, with the keys being press fitted using a transition fit. 

The countershaft has been designed with shoulders that each gear presses up 
against. This prevents any axial movement and helps line up the gears during 
manufacturing and maintenance. The gear hubs also come with holes so set screws
can be inserted, this acts as a clamping mechanism to further prevent any axial
movement by the gears.

All bearings are fitted with interference fits, firmly fixing them in place on 
their respective shafts. Additionally, the housings for the supporting and plain 
bearings are mounted to the gearbox casing, allowing all of the force to be 
distributed throughout the casing.

\begin{table}[htb]
    \centering
    \caption{Component Mounting to the Countershaft} \label{tab:component-mounting}
    \begin{tabular}{lrrrrrr}
        \toprule
        Component Name & Axial Location ($mm$) & Mounting Method \\
        \midrule
        Support Bearing & 0 & Bearing housing and shoulder \\
        N2 Gear & 51 & Profiled key and set screw \\
        N3 Gear & 69 & Profiled key and set screw \\
        Support Bearing & 134 & Bearing housing and shoulder \\
        \bottomrule
    \end{tabular}
\end{table}

To avoid unnecessary stress concentrators, retaining rings have been 
deliberately avoided as a mechanism for mounting components. 

\subsection{Material Selection}

In the application of gearboxes, one of the most critical components is the 
shaft because \textit{all} power passing through the gearbox must go through the
shaft and given the shaft is spinning at such a high speed, any failures can
easily end catastrophically.

The pins used to fasten the gears to the shaft should be designed to fail before
the shaft itself. This is because a pin is relatively cheap and easy to replace,
whereas a broken shaft is considerably more expensive and will cause more damage
to its surroundings. 

As such, the pins will be made of 1020 Steel (yield strength of approximately 
490MPa), while the shaft and gears shall be made of 1040 Steel (yield strength 
around 550 MPa).

To make manufacturing as simple as possible, the casing will be made of cast 
steel. This takes advantage of the relative inexpensiveness of casting compared
to other manufacturing methods like milling directly from billet.

The bearings will also be made out of 1020 Steel due to it being relatively 
inexpensive and because lubrication means there will be minimal metal-to-metal 
contact. Hence there isn't a requirement to use harder materials.

The bearings will be lubricated using industrial grade gear oil, with the exact
grade depending on the working environment, although something similar to SAE 
80W is desired.


\section{Conclusion}

The final gearbox achieved through this design report satisfies the original 
constraints in that it will transfer the specified load and \textit{exactly} 
meets the desired gear ratio. The gear sizes used are also the smallest 
gears physically possible which satisfy the constraints originally set out in 
the \textit{Objectives and Constraints} section.

\vspace{\fill}
\bibliography{references}

\appendix

% Note, I added the front page separately so you can add a \section{} command
% to make the appendix appear in the ToC. You also scale the document down a
% tad to make it fit more nicely
\includepdf[pagecommand=\section{Numerical Analysis}, scale=0.75, pages=1]{./numerical-analysis.pdf}
\includepdf[scale=0.75, pages=2-]{./numerical-analysis.pdf}

\end{document}
