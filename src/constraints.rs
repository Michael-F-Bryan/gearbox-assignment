use Gearbox;

pub fn shafts_are_parallel(gb: &Gearbox) -> bool {
    gb.n1 + gb.n2 == gb.n3 + gb.n4
}

pub struct Ratios {
    lower: f32,
    upper: f32,
}

impl Ratios {
    pub fn new(lower: f32, upper: f32) -> Ratios {
        Ratios { lower, upper }
    }

    pub fn gearbox_within_bounds(&self, gb: &Gearbox) -> bool {
        let ratio = gb.ratio();
        self.lower <= ratio && ratio <= self.upper
    }
}

pub fn within_practical_geartrain_limits(gb: &Gearbox) -> bool {
    let pred = |a, b| a < 10 * b && a < 10 * b;

    pred(gb.n1, gb.n2) && pred(gb.n3, gb.n4)
}

pub fn is_speed_reducer(gb: &Gearbox) -> bool {
    let pred = |pinion, gear| pinion < gear;

    pred(gb.n1, gb.n2) && pred(gb.n3, gb.n4)
}
